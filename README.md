# HN Feed Service

# Deploy

### Linux

- Execute next file from terminal. Make sure the prompt is the same directory that file

```
./start_prod.sh
```

### Windows

- Execute next command from CMD

```
docker-compose -f 'docker-compose.yml' -f 'docker-compose.prod.yml' up -d --build
```

`this command is also valid on Linux environments`

`to run services in development refer to file start_dev.sh`

---

# Environments variables

- for simplicity was uploaded the _.env_ files.
- backend _.env_ is located _backend/posts/.env_
- frontend _.env_ is located _frontend/posts_front/src/environments/_

# Services access

- backend is running by default on _http://localhost:3000/posts_
- frontend is running by default on _http://localhost:4000_
