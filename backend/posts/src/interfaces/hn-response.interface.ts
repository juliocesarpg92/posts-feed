export interface HnResponse {
  hits: Record<string, unknown>[];
  page: number;
  nbPages: number;
  hitsPerPage: number;
}
