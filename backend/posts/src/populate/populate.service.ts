import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression, Interval } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import { HnResponse } from '../interfaces/hn-response.interface';
import { Post } from '../posts/entities/post.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PopulateService {
  private logger: Logger;
  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
  ) {
    this.logger = new Logger(PopulateService.name);
  }

  @Cron(CronExpression.EVERY_HOUR)
  // @Cron(CronExpression.EVERY_5_MINUTES)
  async getPosts() {
    const responseFromApi = await this.fetchFromApi();

    const postsArray: Post[] = [];
    responseFromApi.forEach(async (element) => {
      const post = this.postRepository.create({
        id: element.objectID,
        author: element.author,
        story_id:
          element.story_id !== null
            ? element.story_id
            : Number(element.objectID),
        created_at: element.created_at,
        title:
          element.story_title !== null ? element.story_title : element.title,
        url: element.story_url !== null ? element.story_url : element.url,
      });
      if (post.title !== null) {
        postsArray.push(post);
      }
    });
    await this.saveToDB(postsArray);
    this.logger.log('Done fetching from HN');
  }

  async saveToDB(postsArray: Post[]) {
    let errors = 0;
    for (let index = 0; index < postsArray.length; index++) {
      await this.postRepository.save(postsArray[index]).catch((error) => {
        if (error.code == 11000) {
          errors++;
        }
      });
    }
    this.logger.debug(`there were ${errors} duplicate posts`);
  }

  async fetchFromApi() {
    const pagination = {
      nbPages: Number.MAX_VALUE,
      page: 0,
      hitsPerPage: 100,
    };
    const responsePosts = [];

    while (pagination.nbPages > pagination.page) {
      this.logger.log(`Start ferching page ${pagination.page} from HN`);
      const response = await axios.get(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
        {
          params: {
            hitsPerPage: pagination.hitsPerPage,
            page: pagination.page,
          },
        },
      );
      const hnResponse: HnResponse = response.data;
      responsePosts.push(...hnResponse.hits);
      pagination.nbPages = hnResponse.nbPages;
      pagination.page++;
    }

    return responsePosts;
  }
}
