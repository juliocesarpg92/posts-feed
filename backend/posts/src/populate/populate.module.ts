import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from 'src/posts/entities/post.entity';
import { PopulateService } from './populate.service';

@Module({
  imports: [TypeOrmModule.forFeature([Post])],
  providers: [PopulateService],
  exports: [PopulateService],
})
export class PopulateModule {}
