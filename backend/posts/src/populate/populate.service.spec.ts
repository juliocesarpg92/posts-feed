import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Post } from '../posts/entities/post.entity';
import { PostRepositoryMock } from '../../test/mocks/post.repository.mock';
import { Repository } from 'typeorm';
import { PopulateService } from './populate.service';

describe('PopulateService', () => {
  let service: PopulateService;
  let repository: Repository<Post>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PopulateService,
        {
          provide: getRepositoryToken(Post),
          useValue: PostRepositoryMock,
        },
      ],
    }).compile();

    service = module.get<PopulateService>(PopulateService);
    repository = module.get<Repository<Post>>(getRepositoryToken(Post));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
