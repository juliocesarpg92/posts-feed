import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import {
  postArray,
  PostRepositoryMock,
} from '../../test/mocks/post.repository.mock';
import { Repository } from 'typeorm';
import { Post } from './entities/post.entity';
import { PostsService } from './posts.service';

describe('PostsService', () => {
  let service: PostsService;
  let repository: Repository<Post>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostsService,
        {
          provide: getRepositoryToken(Post),
          useValue: PostRepositoryMock,
        },
      ],
    }).compile();

    service = module.get<PostsService>(PostsService);
    repository = module.get<Repository<Post>>(getRepositoryToken(Post));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should remove post', async () => {
    const testPost = postArray[0];
    const response = await service.remove(testPost.story_id);
    expect(response).toBe(testPost.story_id);
  });

  it('should fail with NotFoundException', async () => {
    expect(service.remove(999999999)).rejects.toBeInstanceOf(NotFoundException);
  });
});
