import {
  Column,
  DeleteDateColumn,
  Entity,
  ObjectID,
  ObjectIdColumn,
  PrimaryColumn,
} from 'typeorm';

@Entity()
export class Post {
  @ObjectIdColumn({
    unique: true,
    primary: true,
  })
  id: ObjectID;

  @Column({
    unique: true,
    type: 'number',
  })
  story_id: number;

  @Column()
  title: string;

  @Column()
  url: string;

  @Column()
  author: string;

  @Column({ type: 'date' })
  created_at: Date;

  @DeleteDateColumn()
  deleted: Date;
}
