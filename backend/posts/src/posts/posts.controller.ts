import { Controller, Get, Param, Delete } from '@nestjs/common';
import { PopulateService } from '../populate/populate.service';
import { PostsService } from './posts.service';

@Controller('posts')
export class PostsController {
  constructor(
    private readonly postsService: PostsService,
    private readonly populateService: PopulateService,
  ) {}

  @Get()
  async findAll() {
    const response = await this.postsService.findAll();
    if (response.length === 0) {
      return this.populateService
        .getPosts()
        .then(() => this.postsService.findAll());
    }
    return response;
  }

  @Delete(':story_id')
  remove(@Param('story_id') story_id: number) {
    return this.postsService.remove(story_id);
  }
}
