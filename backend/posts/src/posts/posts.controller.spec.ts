import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PostRepositoryMock } from '../../test/mocks/post.repository.mock';
import { Repository } from 'typeorm';
import { PopulateService } from '../populate/populate.service';
import { Post } from './entities/post.entity';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';

describe('PostsController', () => {
  let controller: PostsController;
  let repository: Repository<Post>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostsController],
      providers: [
        PostsService,
        PopulateService,
        {
          provide: getRepositoryToken(Post),
          useValue: PostRepositoryMock,
        },
      ],
    }).compile();

    controller = module.get<PostsController>(PostsController);
    repository = module.get<Repository<Post>>(getRepositoryToken(Post));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
