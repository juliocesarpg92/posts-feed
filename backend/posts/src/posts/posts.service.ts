import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Post } from './entities/post.entity';

@Injectable()
export class PostsService {
  private logger: Logger;

  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
  ) {
    this.logger = new Logger(PostsService.name);
  }

  findAll(): Promise<Post[]> {
    return this.postRepository.find({
      order: {
        created_at: 'DESC',
      },
    });
  }

  async remove(story_id: number): Promise<number> {
    const posts = await this.postRepository.find();
    const postToDelete = posts.find((post) => {
      return post.story_id === Number(story_id);
    });

    if (!postToDelete) {
      throw new NotFoundException();
    }
    postToDelete.deleted = new Date();
    const response = await this.postRepository.save(postToDelete);
    this.logger.log(`post with story_id ${story_id} was deleted`);
    return response.story_id;
  }
}
