import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { PostsModule } from './posts/posts.module';
import { ScheduleModule } from '@nestjs/schedule';
import { PopulateModule } from './populate/populate.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      synchronize: true,
      useNewUrlParser: true,
      authSource: 'admin',
      logging: true,
      entities: [__dirname + '/**/*.entity.{ts,js}'],
    }),
    PostsModule,
    PopulateModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
