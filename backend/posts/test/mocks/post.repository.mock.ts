import { Post } from '../../src/posts/entities/post.entity';

const post = new Post();
post.author = 'julio';
post.story_id = 545874;
post.title = 'fake title';
post.url = 'https://fake.url';
post.created_at = new Date();
export const postArray: Post[] = [post];
export const PostRepositoryMock = {
  save: (postToSave: Post) => {
    return Promise.resolve(postToSave);
  },
  find: () => {
    return Promise.resolve(postArray);
  },
};
