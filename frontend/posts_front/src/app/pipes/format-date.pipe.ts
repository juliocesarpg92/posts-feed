import { Pipe, PipeTransform } from '@angular/core';
import * as momentjs from 'moment';

@Pipe({
  name: 'formatDate',
})
export class FormatDatePipe implements PipeTransform {
  transform(value: Date, ...args: unknown[]): string {
    value = new Date(value)

    return momentjs(value).calendar(new Date(), {
      sameDay: 'hh:mm a',
      nextDay: 'hh:mm a',
      nextWeek: 'hh:mm a',
      lastDay: '[Yesterday]',
      lastWeek: 'MMM DD',
      sameElse: 'MMM DD',
    });
  }
}
