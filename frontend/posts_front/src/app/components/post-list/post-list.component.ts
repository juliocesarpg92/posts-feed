import { Component, OnInit } from '@angular/core';
import { Post } from "src/app/interfaces/post";
import { ApiService } from "src/app/services/api.service";

@Component({
  selector: 'post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  posts: Post[]
  isRemovingPost: boolean
  isLoadingPosts: boolean
  constructor(private api: ApiService) { 
    this.posts = []
    this.isLoadingPosts = false
    this.isRemovingPost = false
  }

  ngOnInit(): void {
    this.isLoadingPosts = true
    this.api.getPosts().subscribe((data: Post[]) => {
      this.posts = data
      this.isLoadingPosts = false
    })
  }

  removePost($event: number){
    this.isRemovingPost = true
    this.api.removePost($event).subscribe((data: number) => {
      const found = this.posts.findIndex(post => post.story_id === data)
      this.posts.splice(found,1)
      this.isRemovingPost = false
    })
  }

}
