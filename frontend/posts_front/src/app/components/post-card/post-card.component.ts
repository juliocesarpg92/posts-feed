import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Post } from "src/app/interfaces/post";

@Component({
  selector: 'post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {

  @Input() post!: Post
  public showTrashBtn: boolean
  @Output() removeEvent= new EventEmitter<number>()

  constructor() {
    this.showTrashBtn = false
   }

   setBtnVisibility(visible: boolean){
     this.showTrashBtn = visible
   }

   openUrl(){
     if (this.post.url === null) {
       alert("This post hasn't url attached")
     }
     else{
       window.open(this.post.url, '_blank')
      }
   }

   remove($event: { stopPropagation: () => void; }){
     $event.stopPropagation()
     this.removeEvent.emit(this.post.story_id)
   }


  ngOnInit(): void {
    
  }

}
