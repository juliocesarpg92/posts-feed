export interface Post {
    story_id: number
    title: string
    url: string
    author: string
    created_at: Date
    id: string
}
