import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { PostCardComponent } from './components/post-card/post-card.component';
import { PostListComponent } from './components/post-list/post-list.component';
import { HeaderComponent } from './components/header/header.component';
import { ApiService } from "./services/api.service";
import { FormatDatePipe } from "./pipes/format-date.pipe";

@NgModule({
  declarations: [
    AppComponent,
    PostCardComponent,
    PostListComponent,
    HeaderComponent,
    FormatDatePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
