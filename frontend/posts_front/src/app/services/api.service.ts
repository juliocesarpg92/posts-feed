import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Post } from "../interfaces/post";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  getPosts(): Observable<Post[]>{
    return this.httpClient.get<Post[]>(`${environment.url}`)
  }

  removePost(story_id: number): Observable<number>{
    return this.httpClient.delete<number>(`${environment.url}/${story_id}`)
  }
}
